// Models.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation

protocol JSONEncodable {
    func encodeToJSON() -> AnyObject
}

public class Response<T> {
    public let statusCode: Int
    public let header: [String: String]
    public let body: T

    public init(statusCode: Int, header: [String: String], body: T) {
        self.statusCode = statusCode
        self.header = header
        self.body = body
    }

    public convenience init(response: NSHTTPURLResponse, body: T) {
        let rawHeader = response.allHeaderFields
        var header = [String:String]()
        for (key, value) in rawHeader {
            header[key as! String] = value as? String
        }
        self.init(statusCode: response.statusCode, header: header, body: body)
    }
}

private var once = dispatch_once_t()
class Decoders {
    static private var decoders = Dictionary<String, ((AnyObject) -> AnyObject)>()

    static func addDecoder<T>(clazz clazz: T.Type, decoder: ((AnyObject) -> T)) {
        let key = "\(T.self)"
        decoders[key] = { decoder($0) as! AnyObject }
    }

    static func decode<T>(clazz clazz: [T].Type, source: AnyObject) -> [T] {
        let array = source as! [AnyObject]
        return array.map { Decoders.decode(clazz: T.self, source: $0) }
    }

    static func decode<T, Key: Hashable>(clazz clazz: [Key:T].Type, source: AnyObject) -> [Key:T] {
        let sourceDictionary = source as! [Key: AnyObject]
        var dictionary = [Key:T]()
        for (key, value) in sourceDictionary {
            dictionary[key] = Decoders.decode(clazz: T.self, source: value)
        }
        return dictionary
    }

    static func decode<T>(clazz clazz: T.Type, source: AnyObject) -> T {
        initialize()
        if T.self is Int32.Type && source is NSNumber {
            return source.intValue as! T;
        }
        if T.self is Int64.Type && source is NSNumber {
            return source.longLongValue as! T;
        }
        if source is T {
            return source as! T
        }

        let key = "\(T.self)"
        if let decoder = decoders[key] {
           return decoder(source) as! T
        } else {
            fatalError("Source \(source) is not convertible to type \(clazz): Maybe swagger file is insufficient")
        }
    }

    static func decodeOptional<T>(clazz clazz: T.Type, source: AnyObject?) -> T? {
        if source is NSNull {
            return nil
        }
        return source.map { (source: AnyObject) -> T in
            Decoders.decode(clazz: clazz, source: source)
        }
    }

    static func decodeOptional<T>(clazz clazz: [T].Type, source: AnyObject?) -> [T]? {
        if source is NSNull {
            return nil
        }
        return source.map { (someSource: AnyObject) -> [T] in
            Decoders.decode(clazz: clazz, source: someSource)
        }
    }

    static func decodeOptional<T, Key: Hashable>(clazz clazz: [Key:T].Type, source: AnyObject?) -> [Key:T]? {
        if source is NSNull {
            return nil
        }
        return source.map { (someSource: AnyObject) -> [Key:T] in
            Decoders.decode(clazz: clazz, source: someSource)
        }
    }

    static private func initialize() {
        dispatch_once(&once) {
            let formatters = [
                "yyyy-MM-dd",
                "yyyy-MM-dd'T'HH:mm:ssZZZZZ",
                "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ",
                "yyyy-MM-dd'T'HH:mm:ss'Z'",
                "yyyy-MM-dd'T'HH:mm:ss.SSS"
            ].map { (format: String) -> NSDateFormatter in
                let formatter = NSDateFormatter()
                formatter.dateFormat = format
                return formatter
            }
            // Decoder for NSDate
            Decoders.addDecoder(clazz: NSDate.self) { (source: AnyObject) -> NSDate in
               if let sourceString = source as? String {
                    for formatter in formatters {
                        if let date = formatter.dateFromString(sourceString) {
                            return date
                        }
                    }

                }
                if let sourceInt = source as? Int {
                    // treat as a java date
                    return NSDate(timeIntervalSince1970: Double(sourceInt / 1000) )
                }
                fatalError("formatter failed to parse \(source)")
            } 

            // Decoder for [Account]
            Decoders.addDecoder(clazz: [Account].self) { (source: AnyObject) -> [Account] in
                return Decoders.decode(clazz: [Account].self, source: source)
            }
            // Decoder for Account
            Decoders.addDecoder(clazz: Account.self) { (source: AnyObject) -> Account in
                let sourceDictionary = source as! [NSObject:AnyObject]
                let instance = Account()
                instance.id = Decoders.decodeOptional(clazz: Double.self, source: sourceDictionary["id"])
                instance.name = Decoders.decodeOptional(clazz: String.self, source: sourceDictionary["name"])
                return instance
            }


            // Decoder for [Category]
            Decoders.addDecoder(clazz: [Category].self) { (source: AnyObject) -> [Category] in
                return Decoders.decode(clazz: [Category].self, source: source)
            }
            // Decoder for Category
            Decoders.addDecoder(clazz: Category.self) { (source: AnyObject) -> Category in
                let sourceDictionary = source as! [NSObject:AnyObject]
                let instance = Category()
                instance.categoryID = Decoders.decodeOptional(clazz: Double.self, source: sourceDictionary["CategoryID"])
                instance.name = Decoders.decodeOptional(clazz: String.self, source: sourceDictionary["name"])
                instance.priorCategoryID = Decoders.decodeOptional(clazz: Double.self, source: sourceDictionary["PriorCategoryID"])
                instance.children = Decoders.decodeOptional(clazz: Array.self, source: sourceDictionary["Children"])
                instance.parent = Decoders.decodeOptional(clazz: Category.self, source: sourceDictionary["parent"])
                return instance
            }


            // Decoder for [ChangeResponse]
            Decoders.addDecoder(clazz: [ChangeResponse].self) { (source: AnyObject) -> [ChangeResponse] in
                return Decoders.decode(clazz: [ChangeResponse].self, source: source)
            }
            // Decoder for ChangeResponse
            Decoders.addDecoder(clazz: ChangeResponse.self) { (source: AnyObject) -> ChangeResponse in
                let sourceDictionary = source as! [NSObject:AnyObject]
                let instance = ChangeResponse()
                instance.id = Decoders.decodeOptional(clazz: Double.self, source: sourceDictionary["id"])
                instance.description = Decoders.decodeOptional(clazz: String.self, source: sourceDictionary["description"])
                return instance
            }


            // Decoder for [DeleteResponse]
            Decoders.addDecoder(clazz: [DeleteResponse].self) { (source: AnyObject) -> [DeleteResponse] in
                return Decoders.decode(clazz: [DeleteResponse].self, source: source)
            }
            // Decoder for DeleteResponse
            Decoders.addDecoder(clazz: DeleteResponse.self) { (source: AnyObject) -> DeleteResponse in
                let sourceDictionary = source as! [NSObject:AnyObject]
                let instance = DeleteResponse()
                instance.description = Decoders.decodeOptional(clazz: String.self, source: sourceDictionary["description"])
                return instance
            }


            // Decoder for [ErrorResponse]
            Decoders.addDecoder(clazz: [ErrorResponse].self) { (source: AnyObject) -> [ErrorResponse] in
                return Decoders.decode(clazz: [ErrorResponse].self, source: source)
            }
            // Decoder for ErrorResponse
            Decoders.addDecoder(clazz: ErrorResponse.self) { (source: AnyObject) -> ErrorResponse in
                let sourceDictionary = source as! [NSObject:AnyObject]
                let instance = ErrorResponse()
                instance.message = Decoders.decodeOptional(clazz: String.self, source: sourceDictionary["message"])
                return instance
            }


            // Decoder for [GetAccountsResponse]
            Decoders.addDecoder(clazz: [GetAccountsResponse].self) { (source: AnyObject) -> [GetAccountsResponse] in
                return Decoders.decode(clazz: [GetAccountsResponse].self, source: source)
            }
            // Decoder for GetAccountsResponse
            Decoders.addDecoder(clazz: GetAccountsResponse.self) { (source: AnyObject) -> GetAccountsResponse in
                let sourceDictionary = source as! [NSObject:AnyObject]
                let instance = GetAccountsResponse()
                instance.accounts = Decoders.decodeOptional(clazz: Array.self, source: sourceDictionary["accounts"])
                return instance
            }


            // Decoder for [GetAccountsResponseAccounts]
            Decoders.addDecoder(clazz: [GetAccountsResponseAccounts].self) { (source: AnyObject) -> [GetAccountsResponseAccounts] in
                return Decoders.decode(clazz: [GetAccountsResponseAccounts].self, source: source)
            }
            // Decoder for GetAccountsResponseAccounts
            Decoders.addDecoder(clazz: GetAccountsResponseAccounts.self) { (source: AnyObject) -> GetAccountsResponseAccounts in
                let sourceDictionary = source as! [NSObject:AnyObject]
                let instance = GetAccountsResponseAccounts()
                instance.id = Decoders.decodeOptional(clazz: Double.self, source: sourceDictionary["id"])
                instance.name = Decoders.decodeOptional(clazz: String.self, source: sourceDictionary["name"])
                return instance
            }


            // Decoder for [GetAllCategories]
            Decoders.addDecoder(clazz: [GetAllCategories].self) { (source: AnyObject) -> [GetAllCategories] in
                return Decoders.decode(clazz: [GetAllCategories].self, source: source)
            }
            // Decoder for GetAllCategories
            Decoders.addDecoder(clazz: GetAllCategories.self) { (source: AnyObject) -> GetAllCategories in
                let sourceDictionary = source as! [NSObject:AnyObject]
                let instance = GetAllCategories()
                instance.categories = Decoders.decodeOptional(clazz: Array.self, source: sourceDictionary["categories"])
                return instance
            }


            // Decoder for [GetAllCategoriesCategories]
            Decoders.addDecoder(clazz: [GetAllCategoriesCategories].self) { (source: AnyObject) -> [GetAllCategoriesCategories] in
                return Decoders.decode(clazz: [GetAllCategoriesCategories].self, source: source)
            }
            // Decoder for GetAllCategoriesCategories
            Decoders.addDecoder(clazz: GetAllCategoriesCategories.self) { (source: AnyObject) -> GetAllCategoriesCategories in
                let sourceDictionary = source as! [NSObject:AnyObject]
                let instance = GetAllCategoriesCategories()
                instance.categoryID = Decoders.decodeOptional(clazz: Double.self, source: sourceDictionary["categoryID"])
                instance.name = Decoders.decodeOptional(clazz: String.self, source: sourceDictionary["name"])
                instance.priorCategoryID = Decoders.decodeOptional(clazz: Double.self, source: sourceDictionary["priorCategoryID"])
                return instance
            }


            // Decoder for [InsertionResponse]
            Decoders.addDecoder(clazz: [InsertionResponse].self) { (source: AnyObject) -> [InsertionResponse] in
                return Decoders.decode(clazz: [InsertionResponse].self, source: source)
            }
            // Decoder for InsertionResponse
            Decoders.addDecoder(clazz: InsertionResponse.self) { (source: AnyObject) -> InsertionResponse in
                let sourceDictionary = source as! [NSObject:AnyObject]
                let instance = InsertionResponse()
                instance.id = Decoders.decodeOptional(clazz: Double.self, source: sourceDictionary["id"])
                instance.description = Decoders.decodeOptional(clazz: String.self, source: sourceDictionary["description"])
                return instance
            }
        }
    }
}
